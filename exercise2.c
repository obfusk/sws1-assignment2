#include <stdio.h>

#define MEMPRINT(x) \
  { \
    char *_p = (char *) &x; \
    for (size_t _i = 0; _i < sizeof(x); ++_i, ++_p) { \
      printf("%#016lx %#016x % 16d\n", (size_t) _p, (int) *_p,  (int) *_p); \
    } \
  }

/*
  prints sizes + memory layout of variables
*/
int main ()
{
  short i     = 0x1234;
  char  x     = -127;
  long  sn1   = 413496;
  long  sn2   = 413496; /* all alone :-( */
  int   y[2]  = { 0x11223344, 0x44332211 };

  printf("short i    is %zu byte(s)\n", sizeof(i));
  printf("char  x    is %zu byte(s)\n", sizeof(x));
  printf("long  sn1  is %zu byte(s)\n", sizeof(sn1));
  printf("long  sn2  is %zu byte(s)\n", sizeof(sn2));
  printf("int   y[2] is %zu byte(s)\n", sizeof(y));

  printf("\n");
  printf("%-16s %-16s %-16s\n", "address", "content (hex)", "content (dec)");
  for (int j = 0; j < 16*3+2; ++j) printf("-");
  printf("\n");

  MEMPRINT(i)
  MEMPRINT(x)
  MEMPRINT(sn1)
  MEMPRINT(sn2)
  MEMPRINT(y)

  return 0;
}
