#include <stdio.h>

#define DEVICE "/dev/urandom"
#define ANSWER 42

/*
  reads & prints bytes from DEVICE until ANSWER is found
*/
int main() {
  int c; FILE *f = fopen(DEVICE, "r");
  if (!f) return 1;
  while ((c = getc(f)) != EOF) {
    printf("%d %u %x\n", c, c, c);
    if (c == ANSWER) break;
  }
  fclose(f);
  return 0;
}
