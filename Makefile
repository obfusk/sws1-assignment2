.PHONY: all clean

SHELL     = bash
PROGRAMS  = exercise1b exercise1d exercise2 exercise2_o3 exercise3

CC      = gcc
CFLAGS  = -Wall -Wextra -Werror -std=c99

all: $(PROGRAMS)

clean:
	for p in $(PROGRAMS); do rm -f "$$p"{,.o}; done

exercise2_o3: exercise2.c
	$(CC) $(CFLAGS) -O3 $< -o $@
