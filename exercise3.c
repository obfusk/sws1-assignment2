#include <stdbool.h>
#include <stdio.h>

/*
  print size + representation of bool
*/
int main ()
{
  bool t = true, f = false, x = 42, y = -37, z = 0, o = 1, s = "hi!";

  printf("the size of a bool is %zu byte(s)\n", sizeof(bool));

  printf("true    is 0x%02x\n", t);
  printf("false   is 0x%02x\n", f);
  printf("x (42)  is 0x%02x\n", x);
  printf("y (-37) is 0x%02x\n", y);
  printf("z (0)   is 0x%02x\n", z);
  printf("o (1)   is 0x%02x\n", o);
  printf("s (hi!) is 0x%02x\n", s);

  // it seems that anything but 0 (and false) is interpreted as true

  return 0;
}
