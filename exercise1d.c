#include <stdint.h>
#include <stdio.h>

#define DEVICE "/dev/urandom"
#define ANSWER 42

/*
  reads & prints 2 bytes at a time from DEVICE until ANSWER is found
*/
int main() {
  uint16_t x; int c1, c2;
  FILE *f = fopen(DEVICE, "r");
  if (!f) return 1;
  while (1) {
    if ((c1 = getc(f)) == EOF) break;
    if ((c2 = getc(f)) == EOF) break;
    x = (((uint16_t) (unsigned char) c1) << 8) | ((unsigned char) c2);
    printf("%04x\n", x);
    if (x == ANSWER) break;
  }
  fclose(f);
  return 0;
}
